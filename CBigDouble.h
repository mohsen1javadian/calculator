#include <iostream>
#define maxOfDec 100
#define maxOfInt 1000
using namespace std;

class BigDouble
{
private:
    bool isNegative;//signed value;if is true, number is negative;
    short int *integer,*decimal;
    int lenOfInt,lenOfDec;
public:
    BigDouble(double num=0);
    BigDouble(int lenInt,int lenDec)
    {
        integer=new short int[lenOfInt=lenInt];
        if(lenDec!=0)decimal=new short int[lenOfDec=lenDec];
        else lenOfDec=0;
    }
    BigDouble(BigDouble& num);
    friend ostream& operator <<(ostream& out,BigDouble &num);
    friend istream& operator >>(istream& in,BigDouble &num);
    BigDouble& operator =(BigDouble& num);
    void operator +=(BigDouble& num);
    void operator -=(BigDouble& num);
    void operator /=(BigDouble& num);
    void operator *=(BigDouble& num);
    void operator %=(BigDouble& num);
    friend BigDouble operator +(BigDouble &num1,BigDouble &num2);
    friend BigDouble operator -(BigDouble &num1,BigDouble &num2);
    friend BigDouble operator *(BigDouble &num1,BigDouble &num2); 
    friend BigDouble operator /(BigDouble &num1,BigDouble &num2);
    friend BigDouble operator %(BigDouble &num1,BigDouble &num2);
};
